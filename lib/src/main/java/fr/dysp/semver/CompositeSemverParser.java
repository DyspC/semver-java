package fr.dysp.semver;

import fr.dysp.semver.exceptions.InvalidFormatException;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Parser that delegates to the first of its registered siblings that would accept the input<br>
 * When no registered parser can handle the given data, signals it through {@link #accepts(String)} yielding false
 * and {@link #parse(String)} throwing {@link InvalidFormatException}
 */
public class CompositeSemverParser implements SemverParser {
    
    private final List<SemverParser> parsers;
    
    public CompositeSemverParser(SemverParser... parsers) {
        this.parsers = Arrays.asList(parsers);
    }

    @Override
    public boolean accepts(String text) {
        for (SemverParser parser : parsers) {
            if (parser.accepts(text)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Semver parse(String text) throws InvalidFormatException {
        for (SemverParser parser : parsers) {
            if (parser.accepts(text)) {
                return parser.parse(text);
            }
        }
        throw new InvalidFormatException(String.format("No parser accepted version '%s': %s", 
                text,
                parsers.stream()
                        .map(Object::getClass)
                        .map(Class::getSimpleName)
                        .collect(Collectors.joining(", ", "[", "]"))));
    }
}
