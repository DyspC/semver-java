package fr.dysp.semver.extensions.loose;

import fr.dysp.semver.Semver;
import fr.dysp.semver.SemverParser;
import fr.dysp.semver.exceptions.InvalidFormatException;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Loosens the specification's requirement to have a minor and a patch number <br>
 * Comparison-wise the missing fields are considered to be 0
 */
public class LooseSemverParser implements SemverParser {

    private static final Pattern LOOSE_PATTERN = Pattern.compile(
            "(?<major>" + NUMERIC_IDENTIFIER_PATTERN_FRAGMENT + ")" +
                    "(?:\\.(?<minor>" + NUMERIC_IDENTIFIER_PATTERN_FRAGMENT + ")" +
                    "(?:\\.(?<patch>" + NUMERIC_IDENTIFIER_PATTERN_FRAGMENT + "))?)?" + // major.minor?.patch?
                    "(?:-" + PRERELEASE_PATTERN_GROUP + ")?" + // (-suffix)?
                    "(?:\\+" + BUILD_METADATA_PATTERN_GROUP + ")?", // (+buildmetadata)?
            Pattern.CASE_INSENSITIVE);

    @Override
    public boolean accepts(String text) {
        if (text == null) {
            return false;
        }
        return LOOSE_PATTERN.asMatchPredicate().test(text);
    }

    @Override
    public Semver parse(String text) throws InvalidFormatException {
        if (text == null) {
            throw new InvalidFormatException(null);
        }
        final Matcher matcher = LOOSE_PATTERN.matcher(text);
        if (!matcher.matches()) {
            throw new InvalidFormatException(text);
        }
        int major = Integer.parseUnsignedInt(matcher.group(MatchGroups.MAJOR.getRepr()));
        Integer minor = Optional.ofNullable(matcher.group(MatchGroups.MINOR.getRepr()))
                .map(Integer::parseUnsignedInt)
                .orElse(null);
        Integer patch = Optional.ofNullable(matcher.group(MatchGroups.PATCH.getRepr()))
                .map(Integer::parseUnsignedInt)
                .orElse(null);
        var snapshotParts = parseCompositeIdentifier(matcher.group(MatchGroups.PRERELEASE.getRepr()));
        var metadataParts = parseCompositeIdentifier(matcher.group(MatchGroups.BUILDMETADATA.getRepr()));
        return new LooseSemver(major, minor, patch, snapshotParts, metadataParts);
    }
}
