package fr.dysp.semver.extensions.textual;

import fr.dysp.semver.Semver;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Implements a semver that starts with the prerelease part, this means <ul>
 *     <li>Parts are separated with dots</li>
 *     <li>Parts cannot be empty</li>
 *     <li>There must be at least one part</li>
 *     <li>Numeric parts cannot start by 0</li>
 *     <li>Order is defined as per the prerelease section of the specification</li>
 * </ul>
 * Comparison-wise the missing fields are considered to be 0
 */
public class TextualSemver extends Semver {

    public TextualSemver(List<String> snapshotParts, List<String> metadataParts) {
        super(0, 0, 0, snapshotParts, metadataParts);
    }

    public TextualSemver(List<String> snapshotParts) {
        super(0, 0, 0, snapshotParts);
    }

    @Override
    public int compareTo(Semver o) {
        if (o.getMajor() + o.getMinor() + o.getPatch() > 0) {
            return -1;
        }
        return super.compareTo(o);
    }

    @Override
    public String toString() {
        final StringBuilder repr = new StringBuilder(String.join(DOT, getSnapshotParts()));
        if (!getMetadataParts().isEmpty()) {
            repr.append("+").append(String.join(DOT, getMetadataParts()));
        }
        return repr.toString();
    }

    @Override
    protected float getComparatorPriority() {
        return -10;
    }
}
