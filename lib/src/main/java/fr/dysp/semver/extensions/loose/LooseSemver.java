package fr.dysp.semver.extensions.loose;

import fr.dysp.semver.Semver;

import java.util.List;
import java.util.Objects;

/**
 * Implements a semver that allows minor and patch to be skipped
 * Comparison-wise the missing fields are considered to be 0
 */
// java:S2160 - Added fields are insignificant equals-wise or already handled to Semver#equals through toString
@SuppressWarnings("java:S2160")
public class LooseSemver extends Semver {

    private final boolean hasMinor;

    private final boolean hasPatch;

    public LooseSemver(int major, Integer minor, Integer patch, List<String> snapshotParts, List<String> metadataParts) {
        super(major, Objects.requireNonNullElse(minor, 0), Objects.requireNonNullElse(patch, 0), snapshotParts, metadataParts);
        this.hasMinor = Objects.nonNull(minor);
        this.hasPatch = Objects.nonNull(patch);
    }

    @Override
    public String toString() {
        final StringBuilder repr = new StringBuilder();
        repr.append(getMajor());
        if (hasMinor) {
            repr.append(DOT).append(getMinor());
        }
        if (hasPatch) {
            repr.append(DOT).append(getPatch());
        }
        if (!getSnapshotParts().isEmpty()) {
            repr.append("-").append(String.join(DOT, getSnapshotParts()));
        }
        if (!getMetadataParts().isEmpty()) {
            repr.append("+").append(String.join(DOT, getMetadataParts()));
        }
        return repr.toString();
    }
}
