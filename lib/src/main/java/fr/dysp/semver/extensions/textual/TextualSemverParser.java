package fr.dysp.semver.extensions.textual;

import fr.dysp.semver.Semver;
import fr.dysp.semver.SemverParser;
import fr.dysp.semver.exceptions.InvalidFormatException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextualSemverParser implements SemverParser {

    /**
     * Accepts non-cannon versions such as local-SNAPSHOT branchName-SNAPSHOT
     */
    private static final Pattern TEXT_PATTERN = Pattern.compile(
            PRERELEASE_PATTERN_GROUP + // full text
                    "(?:\\+" + BUILD_METADATA_PATTERN_GROUP + ")?", // (+buildmetadata)?
            Pattern.CASE_INSENSITIVE);

    @Override
    public boolean accepts(String text) {
        if (text == null) {
            return false;
        }
        return TEXT_PATTERN.asMatchPredicate().test(text);
    }

    @Override
    public Semver parse(String text) throws InvalidFormatException {
        if (text == null) {
            throw new InvalidFormatException(null);
        }
        final Matcher matcher = TEXT_PATTERN.matcher(text);
        if (!matcher.matches()) {
            throw new InvalidFormatException(text);
        }
        var snapshotParts = parseCompositeIdentifier(matcher.group(MatchGroups.PRERELEASE.getRepr()));
        var metadataParts = parseCompositeIdentifier(matcher.group(MatchGroups.BUILDMETADATA.getRepr()));
        return new TextualSemver(snapshotParts, metadataParts);
    }
}
