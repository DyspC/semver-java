package fr.dysp.semver;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.regex.Pattern;

/**
 * Strict implementation of the <a href="https://semver.org/">SemVer specification</a>
 * <br><br>
 * <b>Note:</b> Since semvers are defined by their string representation and can hold non-significant data, 
 * this means two different representations can be order-wise equivalent. 
 * Therefore {@link #compareTo(Semver)} returning 0 does not imply {@link #equals(Object)} will be true
 */
public class Semver implements Comparable<Semver> {

    public static final String DOT = ".";

    private static final Predicate<String> NUMERIC_FRAGMENT_PREDICATE = Pattern.compile("\\d+").asMatchPredicate();

    /**
     * @see <a href="https://semver.org/#spec-item-8">semver.org - Major version X</a>
     */
    private final int major;

    /**
     * @see <a href="https://semver.org/#spec-item-7">semver.org - Minor version Y</a>
     */
    private final int minor;

    /**
     * @see <a href="https://semver.org/#spec-item-6">semver.org - Patch version Z</a>
     */
    private final int patch;

    /**
     * @see <a href="https://semver.org/#spec-item-9">semver.org - Pre-release</a>
     */
    private final List<String> snapshotParts;

    /**
     * @see <a href="https://semver.org/#spec-item-10">semver.org - Build metadata</a>
     */
    private final List<String> metadataParts;

    public Semver(Integer major, Integer minor, Integer patch, List<String> snapshotParts, List<String> metadataParts) {
        this.major = major;
        this.minor = minor;
        this.patch = patch;
        this.snapshotParts = Objects.requireNonNullElse(snapshotParts, Collections.emptyList());
        this.metadataParts = Objects.requireNonNullElse(metadataParts, Collections.emptyList());
    }

    public Semver(Integer major, Integer minor, Integer patch, List<String> snapshotParts) {
        this(major, minor, patch, snapshotParts, Collections.emptyList());
    }

    public Semver(Integer major, Integer minor, Integer patch) {
        this(major, minor, patch, Collections.emptyList(), Collections.emptyList());
    }

    /**
     * {@link #major}
     * @return major
     */
    public int getMajor() {
        return major;
    }

    /**
     * {@link #minor}
     * @return minor
     */
    public int getMinor() {
        return minor;
    }

    /**
     * {@link #patch}
     * @return patch
     */
    public int getPatch() {
        return patch;
    }

    /**
     * {@link #snapshotParts}
     * @return snapshotParts
     */
    public List<String> getSnapshotParts() {
        return snapshotParts;
    }

    /**
     * {@link #metadataParts}
     * @return metadataParts
     */
    public List<String> getMetadataParts() {
        return metadataParts;
    }

    /**
     * A wrapper for checking the version is not a prerelease and has {@link #major} &gt; 0
     * @return true if the version should be considered stable
     * @see <a href="https://semver.org/#spec-item-4">semver.org - Major version zero (0.y.z) is for initial development</a>
     */
    public boolean isStable() {
        return major > 0 && snapshotParts.isEmpty();
    }

    /**
     * A wrapper for checking the version is a prerelease
     * @return true if the version is a prerelease
     * @see <a href="https://semver.org/#spec-item-9">semver.org - A pre-release version indicates that the version is unstable and might not satisfy the intended compatibility requirements as denoted by its associated normal version.</a>
     */
    public boolean isSnapshot() {
        return !snapshotParts.isEmpty();
    }

    @Override
    public int compareTo(Semver o) {
        if(o.getComparatorPriority() < getComparatorPriority()) {
            // Swap control since the other class has higher priority
            return -Integer.signum(o.compareTo(this));
        }
        if (!Objects.equals(major, o.major)) {
            return Integer.compare(major, o.major);
        }
        if (!Objects.equals(minor, o.minor)) {
            return Integer.compare(minor, o.minor);
        }
        if (!Objects.equals(patch, o.patch)) {
            return Integer.compare(patch, o.patch);
        }
        if (snapshotParts.isEmpty() && o.isSnapshot()) {
            // The other is the snapshot
            return 1;
        }
        if (isSnapshot() && o.snapshotParts.isEmpty()) {
            // We are the snapshot
            return -1;
        }
        return compareSnapshotParts(o);
    }

    @Override
    public String toString() {
        final StringBuilder repr = new StringBuilder();
        repr.append(major)
                .append(DOT).append(minor)
                .append(DOT).append(patch);
        if (!snapshotParts.isEmpty()) {
            repr.append("-").append(String.join(DOT, snapshotParts));
        }
        if (!metadataParts.isEmpty()) {
            repr.append("+").append(String.join(DOT, metadataParts));
        }
        return repr.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }
        if (!(obj instanceof Semver)) {
            return false;
        }
        return Objects.equals(toString(), obj.toString());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int hash = 17;
        hash = prime * hash + major;
        hash = prime * hash + minor;
        hash = prime * hash + patch;
        hash = prime * hash + snapshotParts.hashCode();
        return prime * hash + metadataParts.hashCode();
    }

    /**
     * Returns a priority value child classes can override to indicate the need of precedence when computing the {@link #compareTo(Semver)} result
     * @return a float, lower means higher priority
     */
    protected float getComparatorPriority() {
        return 0;
    }

    /**
     * Evaluates the defined order on numeric and alphanumeric parts following the first dash in a semver
     * <ol>
     *   <li> Only evaluated when 2 semvers are equal based on their {@link #major}, {@link #minor} and {@link #patch}
     *   <li> Numeric sections are compared in their natural order
     *   <li> Alphanumeric sections are compared by their lexical order in the ASCII table
     *   <li> Numeric sections come before alphanumeric ones
     *   <li> When the above criteria are exhausted, the longest snapshot chain belongs to the latest version
     * </ol>
     * @param o the other semver
     * @return a {@link #compareTo(Semver)} return value based on the snapshot identifiers
     */
    protected int compareSnapshotParts(Semver o) {
        final int comparablePartCount = Math.min(snapshotParts.size(), o.snapshotParts.size());
        for (int i = 0; i < comparablePartCount; i++) {
            if (i >= snapshotParts.size()) {
                // We ran out of parts
                return -1;
            } else if (i >= o.snapshotParts.size()) {
                // They ran out of parts
                return 1;
            }
            final String ours = snapshotParts.get(i);
            final String theirs = o.snapshotParts.get(i);
            int partComparison = singlePartComparison(ours, theirs);
            if (partComparison != 0) {
                return partComparison;
            }
        }

        return 0;
    }

    /**
     * Implements {@link #compareSnapshotParts(Semver)} on a per-part basis
     * @param ours a snapshot fragment belonging to this object
     * @param theirs a snapshot fragment belonging the other object
     * @return a {@link #compareTo(Semver)} return value
     */
    private int singlePartComparison(String ours, String theirs) {
        final boolean numericOurs = NUMERIC_FRAGMENT_PREDICATE.test(ours);
        final boolean numericTheirs = NUMERIC_FRAGMENT_PREDICATE.test(theirs);
        if (numericOurs) {
            if (!numericTheirs) {
                return -1;
            }
            // Check numeric comparison
            final int ourValue = Integer.parseUnsignedInt(ours);
            final int theirValue = Integer.parseUnsignedInt(theirs);
            return ourValue - theirValue;
        } else {
            if (numericTheirs) {
                return 1;
            }
            // Check lexicographic comparison
            return ours.compareTo(theirs);
        }
    }
}
