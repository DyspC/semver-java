package fr.dysp.semver;

import fr.dysp.semver.exceptions.InvalidFormatException;

import java.util.regex.Matcher;

public class StrictSemverParser implements SemverParser {
    @Override
    public Semver parse(String text) throws InvalidFormatException {
        if (text == null) {
            throw new InvalidFormatException(null);
        }
        final Matcher matcher = SPECIFICATION_PATTERN.matcher(text);
        if (!matcher.matches()) {
            throw new InvalidFormatException(text);
        }
        int major = Integer.parseUnsignedInt(matcher.group(MatchGroups.MAJOR.getRepr()));
        int minor = Integer.parseUnsignedInt(matcher.group(MatchGroups.MINOR.getRepr()));
        int patch = Integer.parseUnsignedInt(matcher.group(MatchGroups.PATCH.getRepr()));
        var snapshotParts = parseCompositeIdentifier(matcher.group(MatchGroups.PRERELEASE.getRepr()));
        var metadataParts = parseCompositeIdentifier(matcher.group(MatchGroups.BUILDMETADATA.getRepr()));
        return new Semver(major, minor, patch, snapshotParts, metadataParts);
    }
}
