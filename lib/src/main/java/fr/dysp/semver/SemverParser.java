package fr.dysp.semver;

import fr.dysp.semver.exceptions.InvalidFormatException;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public interface SemverParser {

    /**
     * A regex fragment defining a positive integer with no leading zeroes 
     */
    String NUMERIC_IDENTIFIER_PATTERN_FRAGMENT = "0|[1-9]\\d*";
    /**
     * A regex fragment defining a snapshot part, being either numeric as per {@link #NUMERIC_IDENTIFIER_PATTERN_FRAGMENT} or alphanumeric with dashes
     */
    String PRERELEASE_IDENTIFIER_PATTERN_FRAGMENT = "(?:[0-9a-z-]*[a-z-][0-9a-z-]*|" + NUMERIC_IDENTIFIER_PATTERN_FRAGMENT + ")";
    /**
     * A regex fragment defining a build metadata part, being alphanumeric with dashes
     */
    String BUILD_METADATA_IDENTIFIER_PATTERN_FRAGMENT = "[0-9a-z-]+";

    /**
     * The snapshot regex group, {@link #PRERELEASE_IDENTIFIER_PATTERN_FRAGMENT} sections separated by dots
     */
    String PRERELEASE_PATTERN_GROUP = "(?<prerelease>" + PRERELEASE_IDENTIFIER_PATTERN_FRAGMENT + "(?:\\." + PRERELEASE_IDENTIFIER_PATTERN_FRAGMENT + ")*+)";
    /**
     * The build metadata group, {@link #BUILD_METADATA_IDENTIFIER_PATTERN_FRAGMENT} sections separated by dots
     */
    String BUILD_METADATA_PATTERN_GROUP = "(?<buildmetadata>" + BUILD_METADATA_IDENTIFIER_PATTERN_FRAGMENT + "(?:\\." + BUILD_METADATA_IDENTIFIER_PATTERN_FRAGMENT + ")*+)";

    /**
     * A specification compliant regular expression pattern
     */
    Pattern SPECIFICATION_PATTERN = Pattern.compile(
            "(?<major>" + NUMERIC_IDENTIFIER_PATTERN_FRAGMENT + ")\\.(?<minor>" + NUMERIC_IDENTIFIER_PATTERN_FRAGMENT + ")\\.(?<patch>" + NUMERIC_IDENTIFIER_PATTERN_FRAGMENT + ")" + // major.minor.patch
                    "(?:-" + PRERELEASE_PATTERN_GROUP + ")?" + // (-suffix)?
                    "(?:\\+" + BUILD_METADATA_PATTERN_GROUP + ")?", // (+buildmetadata)?
            Pattern.CASE_INSENSITIVE);

    /**
     * Returns whether the given text can be deserialized by this parser <br>
     * Returning true means {@link SemverParser#parse(String)} must not throw an {@link InvalidFormatException}
     *
     * @param text a non null semver candidate
     * @return true if the parser would return a valid Semver upon parsing
     */
    default boolean accepts(String text) {
        if (text == null) {
            return false;
        }
        return SPECIFICATION_PATTERN.asMatchPredicate().test(text);
    }

    /**
     * Parses a semver candidate
     *
     * @param text a non null semver candidate
     * @return the parsed {@link Semver} object
     * @throws InvalidFormatException if the parser could not resolve the format
     */
    Semver parse(String text) throws InvalidFormatException;

    /**
     * Extracts a dot separated string to a list of parts, making sure the list is valid and does not contain empty strings
     *
     * @param composite the composite part
     * @return the list of parts
     */
    default List<String> parseCompositeIdentifier(String composite) {
        return Optional.ofNullable(composite)
                .map(s -> s.split("\\."))
                .stream()
                .flatMap(Arrays::stream)
                .filter(s -> !s.isEmpty())
                .collect(Collectors.toList());
    }

    /**
     * Holds build time references to the group names managed inside the core library for internal clarity
     */
    enum MatchGroups {
        MAJOR("major"), MINOR("minor"), PATCH("patch"), PRERELEASE("prerelease"), BUILDMETADATA("buildmetadata");

        final String repr;

        MatchGroups(String repr) {
            this.repr = repr;
        }

        public String getRepr() {
            return repr;
        }
    }
}
