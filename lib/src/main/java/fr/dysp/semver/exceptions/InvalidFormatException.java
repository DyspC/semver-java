package fr.dysp.semver.exceptions;

/**
 * When a parser cannot handle the input data it was given
 */
public class InvalidFormatException extends Exception {
    public InvalidFormatException(String message) {
        super(message);
    }
}
