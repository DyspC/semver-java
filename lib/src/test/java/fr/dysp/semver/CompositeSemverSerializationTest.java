package fr.dysp.semver;

import fr.dysp.semver.data.junit.LooseAcceptingDataContextProvider;
import fr.dysp.semver.data.junit.StrictAcceptingDataContextProvider;
import fr.dysp.semver.data.junit.TextualAcceptingDataContextProvider;
import fr.dysp.semver.data.junit.TextualRejectingDataContextProvider;
import fr.dysp.semver.exceptions.InvalidFormatException;
import fr.dysp.semver.extensions.textual.TextualSemverParser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;

class CompositeSemverSerializationTest extends AbstractSemverTest {

    public CompositeSemverSerializationTest() {
        super(new CompositeSemverParser(new StrictSemverParser(), new TextualSemverParser()));
    }

    @TestTemplate
    @ExtendWith(StrictAcceptingDataContextProvider.class)
    void acceptingStrictSamples(String sample) {
        reserializationProducesInputData(sample);
    }

    @TestTemplate
    @ExtendWith(LooseAcceptingDataContextProvider.class)
    void acceptingLooseSamples(String sample) {
        reserializationProducesInputData(sample);
    }

    @TestTemplate
    @ExtendWith(TextualAcceptingDataContextProvider.class)
    void acceptingTextualSamples(String sample) {
        reserializationProducesInputData(sample);
    }

    @TestTemplate
    @ExtendWith(TextualRejectingDataContextProvider.class)
    void rejectingTextualSamples(String sample) {
        InvalidFormatException formatException = Assertions.assertThrows(InvalidFormatException.class, () -> parser.parse(sample));
        Assertions.assertNotNull(formatException.getMessage());
        Assertions.assertTrue(formatException.getMessage().startsWith("No parser accepted version"));
    }

}
