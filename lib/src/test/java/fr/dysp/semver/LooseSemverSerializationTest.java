package fr.dysp.semver;

import fr.dysp.semver.data.junit.LooseAcceptingDataContextProvider;
import fr.dysp.semver.data.junit.LooseRejectingDataContextProvider;
import fr.dysp.semver.data.junit.SemverOrgDataContextProvider;
import fr.dysp.semver.data.junit.StrictAcceptingDataContextProvider;
import fr.dysp.semver.exceptions.InvalidFormatException;
import fr.dysp.semver.extensions.loose.LooseSemverParser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;

class LooseSemverSerializationTest extends AbstractSemverTest {

    public LooseSemverSerializationTest() {
        super(new LooseSemverParser());
    }

    @TestTemplate
    @ExtendWith(SemverOrgDataContextProvider.class)
    void semver_org_examples(String sample) {
        reserializationProducesInputData(sample);
    }

    @TestTemplate
    @ExtendWith(StrictAcceptingDataContextProvider.class)
    void acceptingStrictSamples(String sample) {
        reserializationProducesInputData(sample);
    }

    @TestTemplate
    @ExtendWith(LooseAcceptingDataContextProvider.class)
    void acceptingSamples(String sample) {
        reserializationProducesInputData(sample);
    }

    @TestTemplate
    @ExtendWith(LooseRejectingDataContextProvider.class)
    void rejectingSamples(String sample) {
        InvalidFormatException formatException = Assertions.assertThrows(InvalidFormatException.class, () -> parser.parse(sample));
        Assertions.assertSame(sample, formatException.getMessage());
    }

}
