package fr.dysp.semver;

import fr.dysp.semver.data.junit.SemverOrgDataContextProvider;
import fr.dysp.semver.data.junit.StrictAcceptingDataContextProvider;
import fr.dysp.semver.data.junit.StrictRejectingDataContextProvider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;

class StrictSemverAcceptanceTest extends AbstractSemverTest {

    public StrictSemverAcceptanceTest() {
        super(new StrictSemverParser());
    }

    @TestTemplate
    @ExtendWith(SemverOrgDataContextProvider.class)
    void semver_org_examples(String sample) {
        Assertions.assertTrue(parser.accepts(sample));
    }

    @TestTemplate
    @ExtendWith(StrictAcceptingDataContextProvider.class)
    void acceptingSamples(String sample) {
        Assertions.assertTrue(parser.accepts(sample));
    }

    @TestTemplate
    @ExtendWith(StrictRejectingDataContextProvider.class)
    void rejectingSamples(String sample) {
        Assertions.assertFalse(parser.accepts(sample));
    }

}
