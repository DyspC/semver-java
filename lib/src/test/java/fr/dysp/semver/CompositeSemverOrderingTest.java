package fr.dysp.semver;

import fr.dysp.semver.data.junit.CompositeOrderedDataContextProvider;
import fr.dysp.semver.extensions.loose.LooseSemverParser;
import fr.dysp.semver.extensions.textual.TextualSemverParser;
import fr.dysp.semver.utils.CollectionUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.ArrayList;
import java.util.List;

/**
 * Due to the gitlab branch automatic naming for issues {@link LooseSemverParser} 
 * and {@link TextualSemverParser} can't be both used in the same composite parser if the branch names prefix snapshots
 * (123-my-feature-SNAPSHOT would be considered a prerelease of major 123)
 */
class CompositeSemverOrderingTest extends AbstractSemverTest {

    public CompositeSemverOrderingTest() {
        super(new CompositeSemverParser(new StrictSemverParser(), new TextualSemverParser()));
    }

    @TestTemplate
    @ExtendWith(CompositeOrderedDataContextProvider.class)
    void orderedIsStable(List<String> sample) {
        Assertions.assertIterableEquals(
                sample,
                CollectionUtils.toOrdered(sample, parser));
    }
    
    @TestTemplate
    @ExtendWith(CompositeOrderedDataContextProvider.class)
    void reversedChanges(List<String> sample) {
        List<String> rev = new ArrayList<>(sample.size());
        for (int i = sample.size() - 1; i >= 0; i--) {
            rev.add(sample.get(i));
        }
        Assertions.assertNotEquals(
                rev,
                CollectionUtils.toOrdered(rev, parser));
    }

}
