package fr.dysp.semver.utils;

import fr.dysp.semver.Semver;
import fr.dysp.semver.SemverParser;
import org.junit.jupiter.api.Assertions;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public abstract class CollectionUtils {
    
    public static List<String> toOrdered(List<String> semvers, SemverParser parser) {
        return semvers.stream()
                .map(str -> Assertions.assertDoesNotThrow(() -> parser.parse(str)))
                .filter(Objects::nonNull)
                .sorted()
                .map(Semver::toString)
                .collect(Collectors.toList());
    }
}
