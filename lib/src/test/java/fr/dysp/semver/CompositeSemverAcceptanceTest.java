package fr.dysp.semver;

import fr.dysp.semver.data.junit.LooseAcceptingDataContextProvider;
import fr.dysp.semver.data.junit.StrictAcceptingDataContextProvider;
import fr.dysp.semver.data.junit.TextualAcceptingDataContextProvider;
import fr.dysp.semver.data.junit.TextualRejectingDataContextProvider;
import fr.dysp.semver.extensions.loose.LooseSemverParser;
import fr.dysp.semver.extensions.textual.TextualSemverParser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;

class CompositeSemverAcceptanceTest extends AbstractSemverTest {

    private final SemverParser loose = new LooseSemverParser();
    private final SemverParser strict = new StrictSemverParser();
    private final SemverParser textual = new TextualSemverParser();

    public CompositeSemverAcceptanceTest() {
        super(new CompositeSemverParser(new StrictSemverParser(), new TextualSemverParser()));
    }

    @Test
    void missingPartWorksIfLooseParserIsRegistered() {
        SemverParser parser = new CompositeSemverParser(strict, loose);
        Assertions.assertTrue(parser.accepts("1.2"));
    }

    @Test
    void missingPartFailsIfOnlyStrictParserIsRegistered() {
        SemverParser parser = new CompositeSemverParser(strict);
        Assertions.assertFalse(parser.accepts("1.2"));
    }

    @Test
    void textualWorksIfTextualParserIsRegistered() {
        SemverParser parser = new CompositeSemverParser(strict, textual);
        Assertions.assertTrue(parser.accepts("dev-snapshot"));
    }

    @Test
    void textualFailsIfOnlyStrictParserIsRegistered() {
        SemverParser parser = new CompositeSemverParser(strict);
        Assertions.assertFalse(parser.accepts("dev-snapshot"));
    }

    @TestTemplate
    @ExtendWith(StrictAcceptingDataContextProvider.class)
    void acceptingStrictSamples(String sample) {
        Assertions.assertTrue(parser.accepts(sample));
    }

    @TestTemplate
    @ExtendWith(LooseAcceptingDataContextProvider.class)
    void acceptingLooseSamples(String sample) {
        Assertions.assertTrue(parser.accepts(sample));
    }

    @TestTemplate
    @ExtendWith(TextualAcceptingDataContextProvider.class)
    void acceptingTextualSamples(String sample) {
        Assertions.assertTrue(parser.accepts(sample));
    }

    @TestTemplate
    @ExtendWith(TextualRejectingDataContextProvider.class)
    void rejectingTextualSamples(String sample) {
        Assertions.assertFalse(parser.accepts(sample));
    }

}
