package fr.dysp.semver.data.junit;

import fr.dysp.semver.data.TextualParserTestData;
import org.junit.jupiter.api.extension.Extension;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.TestTemplateInvocationContext;
import org.junit.jupiter.api.extension.TestTemplateInvocationContextProvider;
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver;

import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class TextualOrderedDataContextProvider implements TestTemplateInvocationContextProvider {

    @Override
    public boolean supportsTestTemplate(ExtensionContext context) {
        return true;
    }

    @Override
    public Stream<TestTemplateInvocationContext> provideTestTemplateInvocationContexts(ExtensionContext context) {
        return TextualParserTestData.ORDERED_SAMPLES.stream().map(data -> new TestTemplateInvocationContext() {
            @Override
            public String getDisplayName(int invocationIndex) {
                return String.format("[%d] - Textual ordered sample '%s'", invocationIndex, data);
            }

            @Override
            public List<Extension> getAdditionalExtensions() {
                return Collections.singletonList(new TypeBasedParameterResolver<List<String>>() {
                    @Override
                    public List<String> resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
                        return data;
                    }
                });
            }
        });
    }
}

