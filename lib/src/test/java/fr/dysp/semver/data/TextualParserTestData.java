package fr.dysp.semver.data;

import java.util.List;
import java.util.Set;

public abstract class TextualParserTestData {

    public static final Set<List<String>> ORDERED_SAMPLES = Set.of(
            List.of(
                    // Numeric first part 1
                    "1+-metadata",
                    "1",
                    /// Numeric second part
                    "1.2",
                    "1.2.3-",
                    "1.2.3-.beta",
                    /// Textual second part
                    "1.2+meta-data",
                    "1.2-dev.s-napshot+metadata",
                    "1.2-dev.s-napshot",
                    "1.2C.3", // ascii wise: '-' < 'A' < 'a'
                    "1.2b.3",
                    // Textual first part
                    "1-branch-slug.SNAPSHOT",
                    "1-branch-slug-SNAPSHOT",
                    "1-dev-.snapshot",
                    "1-dev-.snapshot+metadata",
                    "DEV.SNAPSHOT",
                    "DEV-SNAPSHOT"
            ));

    public static final Set<String> ACCEPTS = Set.of(
            "1",
            "1.2",
            "1-dev-.snapshot",
            "1.2-dev.s-napshot",
            "1+-metadata",
            "1.2+meta-data",
            "1-dev-.snapshot+metadata",
            "1.2-dev.s-napshot+metadata",
            "DEV-SNAPSHOT",
            "DEV.SNAPSHOT",
            "1-branch-slug-SNAPSHOT",
            "1-branch-slug.SNAPSHOT",
            "1.2b.3",
            "1.2C.3",
            "1.2.3-",
            "1.2.3-.beta"
    );

    public static final Set<String> REJECTS = Set.of(
            "1.02.3",
            "1.2.3-alpha.0123",
            "1.2.3-alpha.",
            "1.2.3+",
            "",
            "+metadata"
    );
}

