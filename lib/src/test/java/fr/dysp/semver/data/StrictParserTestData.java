package fr.dysp.semver.data;

import java.util.Set;

public abstract class StrictParserTestData {
    
    public static final Set<String> ACCEPTS = Set.of(
            "0.0.0", 
            "1.2.3",
            "123.456.789", 
            "1.2.3-SnApShOt", 
            "1.2.3-alpha.beta.SnApShOt",
            "1.2.3-alpha-beta.SnApShOt", 
            "1.2.3-0alpha.123",
            "1.2.3+develop",
            "1.2.3+Ubuntu.20.04",
            "1.2.3+" + System.currentTimeMillis()
    );
    
    public static final Set<String> REJECTS = Set.of(
            "1.02.3",
            "1..3",
            "1",
            "1.2",
            "1.2b.3",
            "1.2C.3",
            "1.2.3-",
            "1.2.3-alpha.0123",
            "1.2.3-alpha.",
            "1.2.3-.beta",
            "1.2.3+",
            "",
            "+metadata"
    );
}

