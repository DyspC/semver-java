package fr.dysp.semver.data;

import java.util.List;
import java.util.Set;

public abstract class CompositeParserTestData {

    /**
     * For use with a composite parser delegating in this order:<ul>
     *     <li>strict</li>
     *     <li>textual</li>
     * </ul>
     */
    public static final Set<List<String>> ORDERED_SAMPLES = Set.of(
            List.of(
                    // Textual realm
                    "1",
                    "1+-metadata",
                    "1.2+meta-data",
                    "1.2",
                    "1.2.3-",
                    "1.2.3-.beta",
                    "1.2-dev.s-napshot+metadata",
                    "1.2-dev.s-napshot",
                    "1.2C.3", // ascii wise: '-' < 'A' < 'a'
                    "1.2b.3",
                    "1-branch-slug.SNAPSHOT",
                    "1-branch-slug-SNAPSHOT",
                    "1-dev-.snapshot",
                    "1-dev-.snapshot+metadata",
                    "DEV.SNAPSHOT",
                    "DEV-SNAPSHOT",
                    // Strict realm
                    "1.1.5",
                    "2.0.0+final"
            ));
}

