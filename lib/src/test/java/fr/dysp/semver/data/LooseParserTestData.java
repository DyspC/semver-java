package fr.dysp.semver.data;

import java.util.List;
import java.util.Set;

public abstract class LooseParserTestData {

    public static final Set<List<String>> ORDERED_SAMPLES = Set.of(
            List.of("1.9.0", "1.10", "1.11.0"),
            List.of("1.0.0", "2", "2.1", "2.1.1"),
            List.of("1.0-alpha", "1.0.0-alpha.1", "1.0.0-alpha.beta", "1.0-beta", "1.0.0-beta.2", "1.0.0-beta.11", "1.0.0-rc.1", "1")
    );

    public static final Set<String> ACCEPTS = Set.of(
            "1",
            "1.2",
            "1-dev-.snapshot",
            "1.2-dev.s-napshot",
            "1+-metadata",
            "1.2+meta-data",
            "1-dev-.snapshot+metadata",
            "1.2-dev.s-napshot+metadata"
    );

    public static final Set<String> REJECTS = Set.of(
            "1.02.3",
            "1..3",
            "1.2b.3",
            "1.2C.3",
            "1.2.3-",
            "1.2.3-alpha.0123",
            "1.2.3-alpha.",
            "1.2.3-.beta",
            "1.2.3+",
            "",
            "+metadata"
    );
}

