package fr.dysp.semver.data;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class SemverOrgTestData {
    public static final Set<List<String>> ORDERED_SAMPLES = Set.of(
            List.of("1.9.0", "1.10.0", "1.11.0"),
            List.of("1.0.0", "2.0.0", "2.1.0", "2.1.1"),
            List.of("1.0.0-alpha", "1.0.0-alpha.1", "1.0.0-alpha.beta", "1.0.0-beta", "1.0.0-beta.2", "1.0.0-beta.11", "1.0.0-rc.1", "1.0.0")
    );
    public static final Set<String> RANDOM_SAMPLES = Set.of(
            "1.0.0-alpha", "1.0.0-alpha.1", "1.0.0-0.3.7", "1.0.0-x.7.z.92", "1.0.0-x-y-z.--",
            "1.0.0-alpha+001", "1.0.0+20130313144700", "1.0.0-beta+exp.sha.5114f85", "1.0.0+21AF26D3----117B344092BD"
    );

    public static final Set<String> ALL_SAMPLES;
    static {
        Set<String> temp = new HashSet<>(RANDOM_SAMPLES);
        ORDERED_SAMPLES.forEach(temp::addAll);
        ALL_SAMPLES = Set.copyOf(temp);
    }
}

