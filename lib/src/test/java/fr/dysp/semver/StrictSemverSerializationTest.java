package fr.dysp.semver;

import fr.dysp.semver.data.junit.SemverOrgDataContextProvider;
import fr.dysp.semver.data.junit.StrictAcceptingDataContextProvider;
import fr.dysp.semver.data.junit.StrictRejectingDataContextProvider;
import fr.dysp.semver.exceptions.InvalidFormatException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;

class StrictSemverSerializationTest extends AbstractSemverTest {

    public StrictSemverSerializationTest() {
        super(new StrictSemverParser());
    }

    @TestTemplate
    @ExtendWith(SemverOrgDataContextProvider.class)
    void semver_org_examples(String sample) {
        reserializationProducesInputData(sample);
    }
    
    @TestTemplate
    @ExtendWith(StrictAcceptingDataContextProvider.class)
    void acceptingSamples(String sample) {
        reserializationProducesInputData(sample);
    }

    @TestTemplate
    @ExtendWith(StrictRejectingDataContextProvider.class)
    void rejectingSamples(String sample) {
        InvalidFormatException formatException = Assertions.assertThrows(InvalidFormatException.class, () -> parser.parse(sample));
        Assertions.assertSame(sample, formatException.getMessage());
    }

}
