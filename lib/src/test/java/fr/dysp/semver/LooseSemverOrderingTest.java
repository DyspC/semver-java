package fr.dysp.semver;

import fr.dysp.semver.data.junit.LooseOrderedDataContextProvider;
import fr.dysp.semver.data.junit.SemverOrgOrderedDataContextProvider;
import fr.dysp.semver.extensions.loose.LooseSemverParser;
import fr.dysp.semver.utils.CollectionUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.ArrayList;
import java.util.List;

class LooseSemverOrderingTest extends AbstractSemverTest {

    public LooseSemverOrderingTest() {
        super(new LooseSemverParser());
    }

    @TestTemplate
    @ExtendWith(SemverOrgOrderedDataContextProvider.class)
    void orderedIsStable(List<String> sample) {
        Assertions.assertIterableEquals(
                sample,
                CollectionUtils.toOrdered(sample, parser));
    }
    
    @TestTemplate
    @ExtendWith(SemverOrgOrderedDataContextProvider.class)
    void reversedChanges(List<String> sample) {
        List<String> rev = new ArrayList<>(sample.size());
        for (int i = sample.size() - 1; i >= 0; i--) {
            rev.add(sample.get(i));
        }
        Assertions.assertNotEquals(
                rev,
                CollectionUtils.toOrdered(rev, parser));
    }
    
    @TestTemplate
    @ExtendWith(LooseOrderedDataContextProvider.class)
    void extendedSemversAreOrdered(List<String> sample) {
        Assertions.assertIterableEquals(
                sample,
                CollectionUtils.toOrdered(sample, parser));
    }

}
