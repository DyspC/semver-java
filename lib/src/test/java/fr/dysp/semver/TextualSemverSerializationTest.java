package fr.dysp.semver;

import fr.dysp.semver.data.junit.SemverOrgDataContextProvider;
import fr.dysp.semver.data.junit.StrictAcceptingDataContextProvider;
import fr.dysp.semver.data.junit.TextualAcceptingDataContextProvider;
import fr.dysp.semver.data.junit.TextualRejectingDataContextProvider;
import fr.dysp.semver.exceptions.InvalidFormatException;
import fr.dysp.semver.extensions.textual.TextualSemverParser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;

class TextualSemverSerializationTest extends AbstractSemverTest {

    public TextualSemverSerializationTest() {
        super(new TextualSemverParser());
    }

    @TestTemplate
    @ExtendWith(SemverOrgDataContextProvider.class)
    void semver_org_examples(String sample) {
        reserializationProducesInputData(sample);
    }
    
    @TestTemplate
    @ExtendWith(StrictAcceptingDataContextProvider.class)
    void acceptingStrictSamples(String sample) {
        reserializationProducesInputData(sample);
    }

    @TestTemplate
    @ExtendWith(TextualAcceptingDataContextProvider.class)
    void acceptingSamples(String sample) {
        reserializationProducesInputData(sample);
    }

    @TestTemplate
    @ExtendWith(TextualRejectingDataContextProvider.class)
    void rejectingSamples(String sample) {
        InvalidFormatException formatException = Assertions.assertThrows(InvalidFormatException.class, () -> parser.parse(sample));
        Assertions.assertSame(sample, formatException.getMessage());
    }

}
