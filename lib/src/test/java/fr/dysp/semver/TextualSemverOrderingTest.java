package fr.dysp.semver;

import fr.dysp.semver.data.junit.TextualOrderedDataContextProvider;
import fr.dysp.semver.extensions.textual.TextualSemverParser;
import fr.dysp.semver.utils.CollectionUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.ArrayList;
import java.util.List;

/**
 * As per its parsing logic strict semvers may not keep the same order due to the prerelease separator being considered part of a prerelease identifier
 */
class TextualSemverOrderingTest extends AbstractSemverTest {

    public TextualSemverOrderingTest() {
        super(new TextualSemverParser());
    }

    @TestTemplate
    @ExtendWith(TextualOrderedDataContextProvider.class)
    void orderedIsStable(List<String> sample) {
        Assertions.assertIterableEquals(
                sample,
                CollectionUtils.toOrdered(sample, parser));
    }
    
    @TestTemplate
    @ExtendWith(TextualOrderedDataContextProvider.class)
    void reversedChanges(List<String> sample) {
        List<String> rev = new ArrayList<>(sample.size());
        for (int i = sample.size() - 1; i >= 0; i--) {
            rev.add(sample.get(i));
        }
        Assertions.assertNotEquals(
                rev,
                CollectionUtils.toOrdered(rev, parser));
    }

}
