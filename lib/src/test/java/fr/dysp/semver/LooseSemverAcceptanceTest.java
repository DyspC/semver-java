package fr.dysp.semver;

import fr.dysp.semver.data.junit.LooseAcceptingDataContextProvider;
import fr.dysp.semver.data.junit.LooseRejectingDataContextProvider;
import fr.dysp.semver.data.junit.SemverOrgDataContextProvider;
import fr.dysp.semver.data.junit.StrictAcceptingDataContextProvider;
import fr.dysp.semver.extensions.loose.LooseSemverParser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;

class LooseSemverAcceptanceTest extends AbstractSemverTest {

    public LooseSemverAcceptanceTest() {
        super(new LooseSemverParser());
    }

    @TestTemplate
    @ExtendWith(SemverOrgDataContextProvider.class)
    void semver_org_examples(String sample) {
        Assertions.assertTrue(parser.accepts(sample));
    }

    @TestTemplate
    @ExtendWith(StrictAcceptingDataContextProvider.class)
    void acceptingStrictSamples(String sample) {
        Assertions.assertTrue(parser.accepts(sample));
    }

    @TestTemplate
    @ExtendWith(LooseAcceptingDataContextProvider.class)
    void acceptingSamples(String sample) {
        Assertions.assertTrue(parser.accepts(sample));
    }

    @TestTemplate
    @ExtendWith(LooseRejectingDataContextProvider.class)
    void rejectingSamples(String sample) {
        Assertions.assertFalse(parser.accepts(sample));
    }

}
