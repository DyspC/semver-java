package fr.dysp.semver;

import org.junit.jupiter.api.Assertions;

public abstract class AbstractSemverTest {
    protected final SemverParser parser;

    public AbstractSemverTest(SemverParser parser) {
        this.parser = parser;
    }

    protected void reserializationProducesInputData(String text) {
        final Semver parsed = Assertions.assertDoesNotThrow(() -> parser.parse(text), "Unparseable semver: " + text);
        Assertions.assertEquals(text, parsed.toString());
    }
}
