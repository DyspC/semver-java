import java.net.URI

plugins {
    `java-library`
    `maven-publish`
    signing
}

dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter:5.9.2")
    testRuntimeOnly("org.junit.platform:junit-platform-launcher")
}

// Apply a specific Java toolchain to ease working on different environments.
val targetJvm = properties["targetJvm"]?.toString() ?: "11"
java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(targetJvm))
    }
}

tasks.named<Test>("test") {
    // Use JUnit Platform for unit tests.
    useJUnitPlatform()
}

tasks.named<Jar>("jar") {
    manifest {
        attributes(mapOf(
                // Package info
                "Specification-Title" to "Semantic Versioning",
                "Specification-Vendor" to "https://github.com/semver",
                "Specification-Version" to "2.0.0",
                "Implementation-Title" to "fr.dysp:semver",
                "Implementation-Vendor" to "Clement Descamps",
                "Implementation-Version" to version,
                // Build metadata
                "Source-Compatibility" to targetJvm,
                "Target-Compatibility" to targetJvm,
                "Implementation-Build-Epoch" to System.currentTimeMillis(),
                "Created-By" to "${System.getProperty("java.version")} (${System.getProperty("java.vendor")})"
        ), "fr/dysp/semver/")
    }
}

val sourcesJar by tasks.register<Jar>("sourcesJar") {
    archiveClassifier = "sources"
    from(sourceSets.main.get().allSource)
}

val javadocJar by tasks.register<Jar>("javadocJar") {
    archiveClassifier = "javadoc"
    from(tasks.getByPath("javadoc"))
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            from(components["java"])
            artifact(sourcesJar)
            artifact(javadocJar)
            pom { 
                url = "https://gitlab.com/DyspC/semver-java"
                licenses {
                    license {
                        name.set("The MIT License")
                        url.set("https://opensource.org/licenses/MIT")
                    }
                }
                developers { 
                    developer { 
                        id = "dysp"
                        name = "Clément Descamps"
                    }
                }
                scm {
                    connection.set("scm:git:git://gitlab.com/DyspC/semver-java.git")
                    developerConnection.set("scm:git:ssh://gitlab.com:DyspC/semver-java.git")
                    url.set("https://gitlab.com/DyspC/semver-java")
                }
            }
        }
    }
    repositories {
        maven {
            name = "central"
            val repo = if (findProperty("version").toString().endsWith("-SNAPSHOT")) "snapshots" else "releases"
            url = URI("https://s01.oss.sonatype.org/content/repositories/${repo}/")
            credentials {
                username = findProperty("ossrhUsername")?.toString() ?: ""
                password = findProperty("ossrhPassword")?.toString() ?: ""
            }
        }
    }
}

signing {
    sign(publishing.publications["maven"])
}