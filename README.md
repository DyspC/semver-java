# Semver

## Introduction 

This library implements a java parser for the [Semantic Versioning](https://semver.org) specification, with a focus on extensibility, 
ie. bears the responsibility of validating and sorting the data while allowing the interface to be implemented with custom rules
to create version ranges that go in and out of SemVer

## Installation

Replace `VERSION` in the following snippets with a version of your choice you found on maven central, [the release page](https://gitlab.com/DyspC/semver-java/-/releases), or with the latest release ![latest release badge](https://gitlab.com/DyspC/semver-java/-/badges/release.svg?key_width=0&key_text=)

### Using Groovy Gradle

- build.gradle
```groovy
dependencies {
    implementation 'fr.dysp:semver:VERSION'
}
```

### Using Gradle Kotlin DSL

- build.gradle.kts
```kotlin
dependencies {
    implementation("fr.dysp:semver:VERSION")
}
```

### Using Maven

- pom.xml
```xml
<dependency>
    <groupId>fr.dysp</groupId>
    <artifactId>semver</artifactId>
    <version>VERSION</version>
</dependency>
```

## Usage

### Parser implementations

Some parser extensions are built into the jar

| Class                 | Description                                                                                 | Ordering                                                                                                                                                                               |
|-----------------------|---------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `LooseSemverParser`   | Allows skipping minor and patch numbers (eg. `2.0-rc.1`)                                    | Follows the specification considering skipped numbers are 0                                                                                                                            |
| `TextualSemverParser` | Generic fallback to create snapshots from text (eg. `branch-name-SNAPSHOT`, `DEV-SNAPSHOT`) | Those versions are read as prereleases of `0.0.0`, so they come between the minimal `0.0.0-0` and `0.0.0` and follow the mixed numeric/alphanumeric order defined by the specification |

The stricter implementation of the specification is the `StrictSemverParser` class and a resolution chain can be built by feeding any parser sequence to the constructor of `CompositeSemverParser`

### Validating versions

The `accepts` method returns `true` when the parser would return something

```java
        SemverParser semverParser = StrictSemverParser();
        assert semverParser.accepts("1.0.0");
        assert semverParser.accepts("1.0.0-rc.1");
        assert ! semverParser.accepts("1.0");
        assert ! semverParser.accepts("1.0.0.0");
        assert ! semverParser.accepts("text");
```

### Parsing versions

The `parse` method returns a Semver object or throws an `InvalidFormatException` when the `accepts` method returns false

```java
        Semver parsed = semverParser.parse("1.2.3-rc.1");
        assert parsed.getMajor() == 1;
        assert parsed.getMinor() == 2;
        assert parsed.getPatch() == 3;
        assert List.of("rc", "1").equals(parsed.getSnapshotParts());
        try {
            semverParser.parse("text")
            assert false; // unreachable
        } catch (InvalidFormatException e) {
            // did not parse
        }
```

### Generic information

Stability information is processed using the `major` and `prerelease` sections of the semver

```java
        assert semverParser.parse("1.2.3-rc.1").isSnapshot();
        assert ! semverParser.parse("1.2.3-rc.1").isStable();
        assert ! semverParser.parse("0.9.0").isSnapshot();
        assert ! semverParser.parse("0.9.0").isStable();
```


### Comparison

The following examples will be written in kotlin for clarity but can be ported to java using the `Comparable.compareTo` method

```kotlin
val semverParser = CompositeSemverParser(LooseSemverParser(), TextualSemverParser())
assert(semverParser.parse("text") < semverParser.parse("0.0.0"))
assert(semverParser.parse("2.0.0") < semverParser.parse("2.1"))
assert(0 == semverParser.parse("2.0.0").compareTo(semverParser.parse("2.0"))) // Note below about equality and equivalence
```

Note that the main value of a version being its string representation, equivalent versions may not be equal, especially when mixing classes
